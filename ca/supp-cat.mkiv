\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Syscalls \& Unwrapping \type{cat(1)}}]

\startsubject[title={No Wrapping Needed}]

This exercise aims to teach you how to program assembly without the need of any
wrapper.
Hence, your functions entry point (e.g., \type{gauss} or \type{foldl}) must be
the whole programs entry point, since the latter isn't provided for anymore.
The same way, our wrapper required your function name to be exactly as requested,
the operating system requires your functions name to be similarly \quote{as requested}.
The convention for the program entry point name on Linux is \type{_start}.
A minimal example could then look like this:
\startasmcode
global _start

; This allows us to write `sys_exit' everywhere in
; the following program in place of `60'.
%define sys_exit 60

_start:     mov rax, sys_exit
            mov rdi, 0          ; no error
            syscall
\stopasmcode
Note that we didn't actually use \type{ret} at the end of our program, this is
because in fact, Linux doesn't directly \quote{calls} our function.  We thus
need to explicitly tell Linux to please stop the execution of the program with
a system call.
System calls work quite differently then normal function calls.
Usually, every syscall is given a number, which, in \unit{64 bit} x86, is then
written into the \type{rax} register followed by executing the special
\type{syscall} instruction.
The operating systems kernel than reads the \type{rax} register to understand
which syscall you requested.

\startplacetable[title={Linux x86-64 Syscall Convention}]
\starttabulate[|l|l|l|l|l|l|l|l|]
\NC sys no \VL return \VL 1\high{st} arg \NC 2\high{nd} arg \NC 3\high{rd} arg
                      \NC 4\high{th} arg \NC 5\high{th} arg \NC 6\high{th} arg \NC\NR
\HL
\NC rax    \VL rax    \VL rdi            \NC rsi            \NC rdx
                      \NC r10            \NC r8             \NC r9 \NC\NR
\stoptabulate
\stopplacetable

Syscalls also have a different calling convention than regular function calls,
as they pass their arguments in slightly different registers.
Except for the registers used for passing the arguments and the return value,
only \type{rcx} and \type{r11} are overwritten by a syscall (i.e., volatile).
You can find more details about that in the \type{syscall(2)} manual which you
can access on the Linux console using
\starttyping
$ man 2 syscall
\stoptyping

\startplacetable[title={Linux x86-64 Syscall List},location=inner]
\starttabulate[|r|l|l|l|l|l|]
\FL
\NC \#     \NC sys name  \NC return \NC 1\high{st} argument  \NC 2\high{nd} argument \NC 3\high{rd} argument \NR
\ML 
\NC 0      \NC \type{sys_read}  \NC \type{size_t nread}    \NC \type{unsigned int fd}  \NC \type{char *buf} buf   \NC \type{size_t count}    \NC\NR
\NC 1      \NC \type{sys_write} \NC \type{size_t nwritten} \NC \type{unsigned int fd}  \NC \type{const char *buf} \NC \type{size_t count}    \NC\NR
\NC \vdots \NC           \NC    \NC                        \NC     \NC          \NC\NR
\NC 60     \NC \type{sys_exit}  \NC  --                    \NC \type{int error}        \NC --  \NC --       \NC\NR
\LL
\stoptabulate
\stopplacetable
\stopsubject

%\startsubject[title={Program Arguments}]
%If we don't have any wrapper, we of course also need to access the programs
%console arguments, these are passed to our \type{_start} function with the
%signature:
%\startccode
%void _start(int argc, char *argv[]);
%\stopccode
%This means that upon program startup, the register \type{rdi} contains the
%numer (count) of arguments that are passed to our program.
%
%Furthermore, the 
%
%\stopsubject

\startsubject[title={The Con\type{cat}enate Program}]
In theory, the \type{cat(1)} program takes zero to $n$ arguments which are
interpreted as file names and concatenates their contents, printing the
result to the console:
\startnocode
$ cat foo.txt bar.txt baz.txt
I'm a line from foo.txt
I'm two lines ...
... from bar.txt
And another line from baz.txt
\stopnocode

Our simplified version of cat will not take any program arguments.
In that case, both your and original program, will read the consoles interactive
text input, and \quote{echo} it back to the user:
\startplacelisting[reference=lst:icat,title={Interactive Input/Output with \type{cat(1)}}]
\startnocode[lines=yes]
$ cat
I write a line and press enter
I write a line and press enter
The first line was my input, the second by cat(1)
The first line was my input, the second by cat(1)
I can also write a line and press Ctrl-DI can also write a line and press Ctrl-DThis way, no line break is issued
This way, no line break is issued
To terminate input, we need to end the 'input file'
To terminate input, we need to end the 'input file'
We do that by pressing Ctrl-D twice
\stopnocode
\stopplacelisting

The way this works is that the console input is seen as a \quote{file} by
UNIX/Linux programs which contain your text input.
If you type input in the console it will not be \emph{written} to that \quote{file}
immediately though, but only after you \quote{flush the buffer}.
This is done by sending the special ASCII symbol \type{EOT} (End-Of-Transmission)
which can be entered by typing \type{Ctrl-D}.
Alternatively, you can press the \type{ENTER} key which will first append a
newline (\type{'\n'}) at the end of the input and then send \type{EOT}.
The effect can be observed in the \in{Listing}[lst:icat].

As \type{cat(1)} will read the file as it is written and only stop when the file
has \quote{ended}, the console provides an easy way to define the \type{EOF}
(End-Of-File):  Sending \type{EOT} after an empty input does the trick, so
simply sending \type{EOT} twice, consecutively does the trick.

I heavily recommend playing around with the original \type{cat(1)} program a
bit as this behavior is better seen than told.
\stopsubject


\startsubject[title={UNIX File I/O}]

We understand that the console input is actually a file; indeed the program
output is one as well.
These are called \type{stdin} and \type{stdout} or standard input and standard
output respectively.
In UNIX/Linux an open file is simply identified by a number.
Luckily, both \type{stdin} and \type{stdout} are open on program startup with
their ID (called file descriptor, or \type{fd} for short) being \digits{0} and
\digits{1} respectively.

In order to copy from \type{stdin} to \type{stdout} we choose the following
approach:
We copy a certain number of bytes (say, \digits{1024}) from input into a
piece of memory and write them \quote{back} to the output.
We need to note that, if there were less than \unit{1024 Byte} of input, we of
course only want to copy the first few bytes of input.

For that we obviously need some memory of \type{1024 Byte}.
Luckily, our assembler enables us to, quite easily, reserve some storage for
the lifetime of our program and for us to use arbitrarily:
\startasmcode
; this contains all initialized data
section .bss
buffer: resb 1024

; everything after contains the actual code
section .text

_start: ; ...
\stopasmcode
We simply declare a label (which will serve as the memory's start address)
and issue the pseudo-instruction \type{resb} for \quote{reserve bytes} with
the argument \digits{1024}.
In the code, we now can store arbitrary data at \type{[buffer]} to
\type{[buffer+1023]}.

The next thing to do is to actually read those bytes \emph{into} that buffer,
\emph{from} the \quote{file}:
\startasmcode
%define sys_read 0
%define stdin 0

mov rax, sys_read
mov rdi, stdin
mov rsi, buffer
mov rdx, 1024
syscall
\stopasmcode
The function will now read \emph{up to} \unit{1024 byte} from the input file
and to the buffer.
It will return the number of bytes \emph{actually read} in \type{rax}---this
may be less than \unit{1024 Byte}!

Actually, this may even be \digits{0}, as zero input bytes, as already mentioned
earlier, indicates EOF and our program should then terminate:
\startasmcode
; read(...) == 0 ?
cmp rax, 0
je .end
\stopasmcode

The next step is to write those \type{rax} many bytes to \type{stdout} which
works quite similarly.
We simply need to loop this and are done; almost.

The \type{write} function has a small caveat:  Even if we ask it to write
\type{rax} many bytes---it may write less (although, at least \unit{1 B}).
Thus, this function returns the number of bytes it has \emph{actually written}.
With this information, we can implement the following (given in pseudocode):
\startnocode
nread = read(...)
// ...

nwritten = 0
while nwritten < nread:
    nwritten' = write(1, buffer+nwritten,
                      nread-nwritten)
    nwritten = nwritten + nwritten'
\stopnocode
This way, we keep track of the current position of the remaining bytes in the
buffer we need to write (\type{[buffer+nwritten]}) as well as the count
(\type{nread-nwritten}).
\stopsubject

\stopproduct
