\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Control Structures}]

\startsubject[title={Jumps \& Jump Targets}]

For our last program you needed to label the first execution of your code with
\type{gauss:}.
You can think of that label as a named bookmark.
But bookmarks don't really hold any value if we can't use them, so we need a
way to access them.
The easiest way is to simply \emph{jump} to such a bookmark, that is, we have
a special CPU instruction to simply change which instruction is to
be executed next.
\startplacelisting[reference=fig:branch,title={Branch to a label}]
\startasmcode[escape={[[,]]}]
        ; this instruction doesn't have bookmark
        ; and thus will never be jumped to.
        add rbx, 10

foo:    mov rax, 42
        add rax, 10
        [[\tikz[overlay,remember picture] \node (jmp) {};]]jmp bar 
        ; the following instruction won't be
        ; executed either!
        mov rbx, rax
        ; ...

bar:    [[\tikz[overlay,remember picture] \node (target) {};]]mov rdx, rax
        mov rcx, rax
        add rdx, rcx [[\starttikzpicture[overlay,remember picture]\draw[->,color=blue] (jmp) to[bend right] (target);\stoptikzpicture]]
\stopasmcode
\stopplacelisting

Simply put, a jump only overrides the program counter with the new value.
This program counter, in x86 the register \type{rip} for \quote{instruction
pointer}, always holds the address (position) of the next instruction that is
to be executed.
Thus at the beginning of the jump instruction the \type{rip} tells us
that \type{mov rbx, rax} would be up next, as it is the consecutively
next instruction.
Afterwards it points to the instruction after the label \type{bar}, so one could
say \type{jmp bar} is equivalent to \type{mov rip, bar}.

The processor will then simply continue from where it has jumped to.
These simple jumps can also be combined with a condition, yielding conditional
jumps.
If we want to jump only if \type{rax} is larger than \type{rdx} we need to first
instruct the processor to compare both registers.
It will then \quote{remember} a few properties about that last comparison,
e.g., whether the first operand was larger, smaller or equal.\footnote{%
  This is done by saving these properties into the \type{rflags} register.
  The instruction \type{cmp} sets that register, while conditional jumps read it.
}
\startplacelisting[reference=fig:condjmp,title={Conditional Jumps}]
\startasmcode[escape={[[,]]}]
            ; ...
            cmp rax, rdx
            [[\tikz[overlay,remember picture] \node (jmp) {};]]ja above
            mov rcx, rax ; only executed if rax <= rdx
            mov rax, rdx ;     -      ''      -
            mov rdx, rcx ;     -      ''      -

above:      [[\tikz[overlay,remember picture] \node (target) {};]]sub rax, rdx
            ret [[\starttikzpicture[overlay,remember picture]\draw[->,color=blue] (jmp) to[bend right,out=310,in=220] (target);\draw[->,color=red] (jmp) to (target);\stoptikzpicture]]
\stopasmcode
\stopplacelisting

You can see in \in{Figure}[fig:condjmp] that if the condition is not true, the
processor carries on executing the next instruction.
It will do so until the next jump or \type{ret} instruction is found:  This means
that it will eventually reach the \type{above} label in our case and execute
the instructions as well!\footnote{%
  I use the \type{ja} instruction for comparison.  There's also the \type{jg}
  instruction which is different and \emph{wrong} for our use case.
  For the time being, use \type{ja}, \type{jae}, \type{jb} and \type{jbe} for
  $>$, $\geq$, $<$ and $\leq$.
}

Unconditional and conditional jumps are crucial to implement standard control
flow primitives such as loops or if-else and switch-case structures.
However, there's another important \quote{jump} which is the \type{call}
instruction paired with \type{ret}, which we've used already.
We will learn about that in a later session in more detail, but for now the
following should help clear up some points.
A regular jump is a one-way road, there's no way to (easily) go back to our
jump source---without somehow remembering that point and then jumping there.

The solution is the \type{call}/\type{ret} combination which basically does
exactly that:  We can use \type{call my_label} and the processor will
\quote{remember} our origin, s.t., when it finds a \type{ret} instruction it
will \quote{go back}.
A label that is to be used with \type{call} is then called a function and this
is precisely the reason why we needed to put the return instruction at the
\quote{end} of our code, as it \quote{switches back} to our wrapper.\footnote{%
  You could also put it in the middle and jump to it, if you want, it just
  makes code less readable.
}
\stopsubject


\startsubject[title={Control structures}]

In other imperative higher programming languages, we usually have primitive
control structures, such as:
\startccode
if (P) {
    /* if condition P is true */
} else {
    /* if P is false */
}
/* unconditional remaining code */

while (P) {
    /* as long as P is true: This is likely code
     * that may modify P
     */
}
\stopccode
Or the more complex \type{for}-loop.  We can use our jumps together with the
comparison instruction to build these primitives in Assembly.

To do that, we need to add labels for all blocks of codes that we may want to
jump to (\type{.iftrue} is not really needed):\footnote{%
  I prefixed these labels with \type{.} as these are \quote{local labels}
  that belong to \type{foo}. That way I can have another function \type{bar}
  with its own local labels \type{bar.else} etc.
}
\startasmcode[escape={[[,]]}]
foo:        ; ...
            cmp [[{\em op1, op2 }]]
            jn[[{\em CC}]] .else    ; n negates
.iftrue:    ; if condition is true
            ; ...

            jmp .afterif
.else:      ; if condition is false
            ; ...

.afterif:   ; unconditional remaining code
            ; ...
\stopasmcode

This way, we can compare the operands and if the condition specified
is evaluated to true, we will not jump, as the \quote{n} specifies
negation.  However, if it is false, we jump to the \type{.else} block.
The negation is a feature of the assembler, it translates \type{ja} to the
same machine operation code as \type{jnbe} (jump-not-below-or-equal).
Instead, you can also simply negate the given condition or reorder the blocks.

Now it's your turn:  How would you implement a loop?
\stopsubject

\stopproduct
