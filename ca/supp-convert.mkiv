\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Strings \& Number Representation}]

\startsubject[title={On Numbers}]

While it may seem obvious what a number \quote{is}, this question isn't as
trivial as one might think.
Take the example of $1000$:  Usually we would see this as \quote{one thousand}
but it could, just as well, be \quote{eight} if we assume base two (binary)
instead of base \digits{10} (decimal).
So while \quote{eight} and \quote{one thousand} are different numbers, they
may be represented by the same \emph{string of digits} in different bases.
Thus, it is always crucial to know which base we are in, even if we assume
decimal by default.

Furthermore, we can represent the number \quote{ten} as $10$ (decimal) and as
$A$ (hexadecimal or sedidecimal).
To be able to distinguish these, there are many conventions such as writing
the base in subscript or using prefixes, i.e., $0\mathrm{x}A$ or $A_{16}$.
Regardless of the form used, noting down a number in a specific format is called
a \emph{representation}.
The most common representation concept (which is also what we've used here) is
\emph{positional notation}, however there are different ways, such as roman
numerals.
In a positional system, a number with a given base $b$ is represented as
a string of digits with an optional sign-prefix $-$ or $+$:
\startformula
  \pm d_n d_{n-1} \;\cdots\; d_1 d_0
\stopformula
The value of the above representation with $n$ digits can then be calculated
as follows:
\startformula
(-1)^s \cdot \sum_{i=0}^n d_i b^i = (-1)^s \cdot d_0 b^0 + d_1 b^1 + \cdots + d_n b^n
\stopformula
With $s=1$ if the sign was $-$, and otherwise $0$.

This means a digit in a different position has a different power of its base
attached to it, hence positiona notation.
In the current exercise you are asked to convert such a representation into
the integer, i.e., evaluate the above formula for a given sequence of digits
$d_i$ and a base $b$.
\stopsubject


\startsubject[title={Strings \& Encodings}]

We now want to understand in what format that digit sequence is given to us.
As these strings may be arbitrarily long, it is impractical to pass them to our
program in registers as these can only hold a very limited amount of data with
a fixed length.
What is however, at least as far as we are considered, virtually unlimited is
main memory:  Remember, we've yet not explicitly dealt with main memory and only
worked on the CPUs registers!

While registers have names, memory is divided into many bytes which are being
numbered starting from zero.
The number of the byte is it's address.\footnote{%
  Common sizes of bytes are \unit{8 bit} but there are differently sized bytes out
  there.
}

We can now \quote{point} to a memory location by noting down the address of
the starting byte.
In fact, that's exactly how strings of characters (in C and Assembly) work.
Imagine our memory location is \digits{1024} with this byte having the value
\digits{49}.  The following byte, \digits{1025}, has the value \digits{50}
and \digits{1026} the value \digits{51}.  Finally, byte \digits{1027} has the
value \digits{0}.  There are obviously bytes afterwards, but there values
don't matter to us for reasons we will see soon.

Each of these bytes \emph{encode} a \emph{symbol} that is e.g., a letter from
a--z or in upper case from A--Z, punctuation or a decimal digit \digits{0}--\digits{9}.
One could imagine the symbol \digits{0} being encoded with the byte of value
\digits{0}, but that is usually not the case.
The encoding that we will assume and work with, ASCII, encodes this character
with the byte of value \digits{48}, with \digits{49} encoding the \digits{1}
character.
The ASCII value of \digits{0} instead encodes the special \type{NUL} character
which denotes \emph{end of string}.

So, given the above string consisting of the bytes \type{[49, 50, 51, 0]}, we
can understand that it's the ASCII coding of \type{"123"}!

Thus, in order for anyone to pass around arbitrarily long sequences of
characters (be it digits or anything else) can just dump these into memory,
coded in ASCII, add a \type{NUL} character, and pass out the starting address.
\stopsubject


\startsubject[title={Putting it together}]

% int64_t  strToInt(const  char *str , uint8_t  base);
The calling convention still demands that the first argument is passed in
\type{rdi}.
As this argument is a, possibly very long, string, it will not contain the
whole digit sequence but just the starting address of this sequence.

In order to access the first digit in that string we need to somehow access
memory.
To load one byte at the address stored in \type{rdi} we can do the following:
\startasmcode
mov al, [rdi]
\stopasmcode
There are two new things here: First, we put the register in brackets.  This
serves us to distinguish moving the value within the register into the destination
from copying the value \emph{at the address} given in the register.
The computer has no way to know that the number \type{rdi} holds is a \quote{pointer}!

The second new thing is the register \type{al}.  It's not that new though, as
\type{al}---\type{a} lower---simply refers to the lowest byte of the \unit{8 byte}
register \type{rax}.
We can also access the second-lowest byte of \type{rax} with \type{ah}, both
bytes together as a \unit{2 byte} register \type{ax} (\type{a} extended) and
the lower \unit{4 byte} of \type{rax} with \type{eax} (\type{a} extended, extended).
\startnocode
ax  = ah:al
eax = [2 bytes]:ax
rax = [4 bytes]:eax
\stopnocode
The same pattern works for \type{rbx} through \type{rdx} and there are similar
ways to access the other parts of the other registers as well.

With that, we can decode the given line of code to \quote{copy one byte from
address given in \type{rdi} to the lowest byte of \type{rax}}.
We could have used \type{rax} in place of \type{al}, but then we'd have copied
not just one byte but \type{8 byte} when we wanted just one character.

The second argument, passed in \type{rsi}, is only one byte big as well, so
instead of using \type{rsi} we need to use \type{sil}.

You now know how to access every byte from our input sequence and know where
the base is stored.
Try implementing an algorithm to evaluate the sum mentioned in the first part.

\stopsubject

\stopproduct
