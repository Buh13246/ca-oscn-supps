\startproduct supp-sockets
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title=Supplement: Connecting Processes over TCP]

\startsubject[title=From UNIX IPC to TCP/IP]

The novel concept of using sockets for communication
between processes on the same system, as we did with UNIX Domain Sockets in the last exercise, can
be easily adapted to enable communication between processes
\emph{across} computers: Within a computer network.

There are many ways computers can communicate with each other but in this
exercise we will restrict ourselves to the TCP/\hskip0pt IP protocol stack.
From a programming point of view, this is an evolutionary change:
Virtually the only difference is that our socket is not backed anymore by a \quote{file}
on the disk\footnote{%
  This also implies you don't need \type{unlink(3)} anymore!
} representing the \quote{address} via its path (but it is still accessed via
a file descriptor!).
Instead the server needs to know computers IP within the network and allocate a
Port number to listen on.
The IP describes the computer itself, the port determines the specific
service/\hskip0pt server on
the computer (as one computer can have multiple servers running).

Thus the invocation of our server program on the command line 
changes from passing the socket name to
passing IP\footnote{%
  We pass the IP because the computer might be connected to multiple networks
  simultaneously, each giving it a different IP! More on that later.
} \& port:
\startccode
$ ./server <address> <port>
\stopccode
The client is adapted analogously.
%
\placefloats
\stopsubject

\startsubject[title=IP Addresses]

The IPv4 and IPv6 protocols specify the format and semantics of addresses, examples
include:
\startitemize
\item \type{192.168.1.}$x$ or \type{192.168.100.}$x$: common home network IPv4
\item \type{160.45.112.11}: IPv4 of andorra at time of writing
\item \type{8.8.8.8}: Google IPv4 DNS
\item \type{127.0.0.1} and \type{::1}: IPv4/IPv6 localhost on loopback network
\stopitemize
In general, even a device that's not connected to any real network has a special
\quote{loopback} network device that\dots\@ loops back to itself.
Say your laptop is logged in to the universities wireless network and simultaneously
connected to a different network via LAN\@ as seen in \in{Figure}[fig:networks]. 
%
\startplacefigure[reference=fig:networks,title=A PC in 3 IPv4 networks (incl.\@ loopback)]
\hbox{%
\starttikzpicture[auto]
\node[draw] (h) {Your PC};
\node[draw,below left=of h] (r1) {Router 1};
\node[draw,right=of r1] (a) {Alice PC};
\node[draw,below=of r1] (b) {Bob PC};
\node[draw,above right=of h] (r2) {Router 2};
\node[draw,below right=of r2] (c) {Charlie PC};

\node[draw,dashed,fit=(h) (r1) (a) (b),label={below:Network 1}] (N1) {};
\node[draw,dashed,fit=(h) (r2) (c),label={below:Network 2}] (N2) {};

\draw (r1) -- (h);
\draw (r1) -- (a);
\draw (r1) -- (b);

\draw (r2) -- (h);
\draw (r2) -- (c);

\draw (h) edge[out=135,in=165,loop] node[left] (lh) {\type{127.0.0.1}} (h);
\node[draw,dotted,fit=(h) (lh)] {};

\stoptikzpicture
}
\stopplacefigure
%
It will have three IP addresses: One for the university network (e.g.\@
Network~1), one for the other wired connected network (e.g.\@ Network~2) and
another for the pseudo loopback network.
While the other two IPs are usually assigned via the router (e.g.\@ using DHCP),
the localhost IPv4 is always \type{127.0.0.1}.\footnote{%
  There are other ways to construct networks and assign IPs, you don't even
  really need a router.
}
This enables us to even use TCP/IP for communication between processes on the
same system, as well as using it for remote systems.

It is crucial to understand the IP is not unique for one system, but only unique
\emph{within} a single network.
If a process on Alice~PC wants to connect to a process on Your PC it will need
the IP that assigned to your PC within the Network~1.
This might be the same
or a different IP than the IP assigned to Your PC by Router~2 in Network~2!%
\footnote{%
  One could ask: But what if Alice and Charlie have the same IP, how do we
  distinguish those?
  For that (and other cases) we need routing information, e.g.\@ type
  \type{ip route} on your Linux shell to see your current setup (don't be afraid
  if the output is confusing).
}

In this exercise you can test the easiest by simply using localhost
\type{127.0.0.1} as
the IP of your device and some port number above or equal to 1024.
If you have two devices in the same network available, you can try connecting
these.
Note however that you might need to configure firewalls for allowing packets to
actually arrive.
\placefloats
\stopsubject

\startsubject[title=IP Sockets \& Blocking Connections]

In the last exercise we used sockets of the type \type{AF_UNIX}, now we will
use IPv4 sockets, i.e.\@ of type \type{AF_INET}.
While we needed to copy our sockets filename into the \type{struct sockaddr_un}
structure before, we now need to fill the \type{struct sockaddr_in} structure
described in \goto{\type{netinet/in.h(0)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/netinet_in.h.html)].
This requires our IP and Port not to be given as strings but as \type{struct in_addr}
and \type{in_port_t}, respectively.
Luckily, there are functions to convert an IP in dotted-decimal notation (see examples)
into
such an IP structure (\goto{\type{inet_pton(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/inet_ntop.html)]),
as well as converting a port given as some unsigned integer into the required port type (\goto{\type{htons(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/htonl.html)]).%
\footnote{%
  Why do we need to call \type{htons(3)} and what does this name even mean?
  This has to do with the so-called network byte-order or endianness.
  If you have two consecutive \unit{2 Byte} values, Byte 1--4, you could order
  these as B1, B2, B3, B4 or B2, B1, B4, B3, that is the bytes of each value
  are written \quote{backwards}.
  This has many useful properties, however computer architectures differ in
  their implementation.
  In order to be communicate across multiple architectures a network byte order
  has been standardized and we call host-to-network-short to guarantee our short
  (\unit{2 Byte}) value is in the correct byte order.
}

That's it, we've adapted our local UNIX IPC client and server to be able to communicate
over network using TCP/\hskip0pt IP!
However, when viewing connection-oriented systems, we have another issue to deal with:
As soon as someone connects, the whole system is blocked.
It would be rather bad if only one person could access a website at a time,
telling everyone to wait until the current client is finished.
In the previous exercise we explicitly allowed that behavior, we now want to
fix this issue.

To solve it we need to have another closer look at the funciton
\goto{\type{accept(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/accept.html)].
In fact, this function does not simply \quote{pick up the phone} but actually
only acts as some kind of \quote{secretary}.
It accepts the connection \emph{and creates
a new socket}, forwarding the \quote{call} to it s.t.\@ this
new \emph{connection} socket is the one actually used for further communication
with our client
(Step 2 in \in{Figure}[fig:multiconn]).
The original \emph{listen} socket can still be \quote{called}, but currently
our process is busy talking and can't \quote{pick up}.

Ideally, we find a way to accept new connections (Step 3) while talking to the
clients, that way our system is only blocked for as long as the
\quote{secretary} needs to forward the \quote{call}.
%
\startplacefigure[reference=fig:multiconn,title=Connection-Oriented Sockets with a Listener]
\hbox{%
\starttikzpicture[auto]
\node[draw] (srv) {listener};
\node[draw,above left=of srv] (w1)  {worker-1};
\node[draw,left=of srv] (w2)  {worker-1};
\node[draw,above right=of srv] (cl1) {client-1};
\node[draw,right=of srv] (cl2) {client-2};

\draw[->,dashed] (cl1) edge node[above] {(1)} (srv);
\draw[->] (cl1) edge node[above] {(2)} (w1);
\draw[->,dotted] (srv) -- (w1);
\draw[->] (cl2) edge node {(3)} (srv);
\stoptikzpicture
}
\stopplacefigure

There are two ways to listen for new connections while talking to a client that
we will both cover, but you need only implement one!

\placefloats
\stopsubject

\startsubject[title=Extend using \type{fork(3)}]

The first way to deal with this problem is to actually have multiple processes
as part of our server program, one for listening and one for each connection to
a client.\footnote{%
  This is not using threads, but whole processes!  Threads would be another
  way to deal with this, but we don't cover that here.
}
Specifically, when we start our program, our OS spawns one process as an
instance of our program.
However, this running process can then spawn another process from itself, effectively
creating an almost identical copy.
This technique is called \quote{\goto{\type{fork(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/fork.html)]ing}
and the way to distinguish our process copy, called the \quote{child},
from the orignal \quote{parent}, is to look at the return value of the \type{fork(3)}
system call, as it's different for parent and child.
%
\startplacelisting[title=Example using \type{fork(3)}]
\startccode
pid_t pid = fork();
if (pid == 0) {
    printf("We are the child\n");
    // child functionality here ...
} else {
    printf("We are the parent, child is %ld\n", pid);
    // parent functionality here ...
}
\stopccode
\stopplacelisting
%
More specifically the variable holds the process ID of our child \emph{if we are the
parent}, and the value $0$ otherwise.

This means that our program contains both, the code of our child (or even children)
and of the parent process, and we check with our if-statement which part of our
program is to be executed by each process.
%
\startplacefigure[reference=fig:fork,title=State diagram for \type{fork(3)}]
\hbox{%
\starttikzpicture[auto,node distance=2cm]
\node (m) {\type{main()}};
\node[node distance=1cm,right=of m] (f) {\type{accept(); fork()}};
\node[right=of f] (p) {\type{continue}};
\node[above right=of f] (c) {\type{read()}/\type{write()}};

\draw[->] (m) -- (f);
\draw[->] (f) to node[below] {\type{pid != 0}} (p);
\draw[->] (p.south east) to[out=340,in=200] (f.south west);
\draw[->] (f.north east) to node[sloped] {\type{pid == 0}} (c.south west);

\stoptikzpicture
}
\stopplacefigure

The idea is now to have our infinite loop run in our parent process as you can
see in \in{Figure}[fig:fork].
Each time it \type{accept(3)}s a connection by a client it \type{fork(3)}s to
create a new process that actually deals with the connection.
Meanwhile the parent immediately goes back to wait for new incoming connections
it can \type{accept(3)}.

\placefloats
\stopsubject

\startsubject[title=Extend using \type{select(3)}]

Instead of using processes running in parallel, we can use our one existing process
to regularly walk over all sockets (the listen socket and the connection sockets),
polling each whether new data is available.
This technique, called synchronized I/O Multiplexing, is often implemented using
\goto{\type{select(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/poll.html)].\footnote{%
  Another alternative is to use \goto{\type{poll(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/poll.html)]
  but we will not cover it, it's functionally equivalent.
}
This function introduces a data type called \type{fd_set} which can hold multiple
file descriptors (i.e.\@ our sockets) and is modified and read through the macros
\type{FD_ZERO}, \type{FD_SET}, \type{FD_CLR} and \type{FD_ISSET}.
At first, we only have one socket, the listen socket in our structure, so
we do the following:
\startccode
fd_set fds;
FD_ZERO(&fds);
FD_SET(listen_sock, &fds); // add it to the set
\stopccode

The \quote{walking over all sockets} happens in two steps.
First call our \type{select(3)} function, passing our set of file descriptors.
But it needs another information to work: The highest fd in the set, plus one:
\startccode
int max = listen_sock; // the only one in the set
fd_set activefds = fds;
select(max+1, &activefds, NULL, NULL, NULL);
\stopccode
This code will block (do nothing) as long as no fd in the set receives data.
As soon as some sockets are \quote{active} in the sense that they received
data, the function will remove all other, inactive sockets from the set
(hence we copy it beforehand).
Now we have a set of all sockets and a set of those that are ready to be read.

We now can loop over all \emph{possible fds}, discarding those that aren't part of the
set:
\startccode
// fds are just integers!
for (int fd = 0; fd < max+1; fd++) {
	if (!FD_ISSET(fd, &activefds)) {
		continue;
	}
	/* fd is 'ready' */
}
\stopccode
As soon as we've found a fd ready to be read, we need to distinguish two cases:
Is this fd our listen socket, or is it some connection socket?
We can simply check whether \type{fd == listen_sock} is true and then
accept the new connection, yielding a new connection socket in return.
This socket needs to be added to our original fd set \emph{and \type{max} needs
to be updated!}

Otherwise we simply handle the connection, echoing the clients message on the
server side.
If the client asks the server to hang up the connection, we \type{close(3)} the
connection socket and remove the respective fd from our set.\footnote{%
  We don't need to update \type{max}.
}

Note: This option is not \quote{as parallel} as using processes, as we are busy as
long as we read data from a client.
However, hopefully, the consecutively sent data from the client is rather short,
and we can go back to polling everyone rather quickly.

%
%\startplacefigure[reference=fig:select,title=Ablauf von \type{select(3)}]
%\hbox{%
%\starttikzpicture[auto]
%\node[] (s) {\type{select()}};
%\node[right=of s] (f) {\type{for (fd ...)}};
%\node[above right=of f] (l) {\type{accept()}};
%\node[below right=of f] (c) {\type{read()}/\type{write()}};
%\node[above right=of c] (f') {};
%
%\draw[->] (s) to (f);
%\draw[->] (f) |- node[sloped] {(a)} (l);
%\draw[->] (f) |- node[sloped] {(b)} (c);
%\draw[->] (l) -| (f');
%\draw[->] (c) -| (f');
%\draw[->] (f') to[bend left,out=0,in=180,looseness=3] (f);
%%\draw[->] (f.south east) to[out=340,in=200] (s.south west);
%%\draw[->] (f.north east) to node[sloped] {\type{pid == 0}} (c.south west);
%
%\stoptikzpicture
%}
%\stopplacefigure


\stopsubject
\placefloats

\stopproduct
