\startproduct supp-sockets
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title=Supplement: IPC with UNIX-Domain-Sockets]

\startsubject[title=Server-Client Principle \& Protocols]

A common principle in system architecture is splitting functionality into
a server and a client.
The server provides some functionality to the clients that connect to it.
This pattern can be used to let processes communicate on the same computer as
well as over a network.\footnote{%
  A rather user-visible example is a web (or HTTP) server with the client being
  the browser you use to access it.

  But Microkernels also embrace this idea to provide
  different functionality as small server processes in userspace that are orchestrated
  by the kernel proper.
}
The \emph{critical} part is the communication:
How does the client know \quote{where} the server is?
What \quote{language} do both parties speak?

These questions are answered by protocols.
But just like the applications themselves, the protocols aren't monolithic but
modular as well.
We can have a protocol that handles the address resolution (think: Calling
Alice who has Bobs number and ask her for his), the addressing itself (Alices \&
Bobs number) and one to actually make the connection (dialing, picking up).
We can even have additional protocols that are less set in stone, like saying
\quote{Hello, Alice/Bob here} when picking up the phone.

The protocol stack of the Internet would consist of IP (the number) and TCP
(calling~/ picking up) with DNS (Alice in this case) and eg.\@ HTTP sitting \quote{above}.
Below IP there are other layers which define the physical transport from
one networked device to another.
%
\placefloats
\stopsubject

\startsubject[title=Connection-oriented vs.\@ Connection-less]

Furthermore we can broadly distinguish server/client systems into
connection-oriented and connection-less protocols.
While connection-oriented protocols compare to phonecalls above,
connection-less protocols can be viewed as packages sent by post: They don't
need a stable connection, but they don't necessarily arrive on time, in order
or at all. However, and this is where the analogy is unrealistic, they might be
faster delivering the information.\footnote{%
  Well, it \emph{is} probably faster to pack three books into a packet and send it
  rather than reading them over telephone and transcribing them on the other
  end.
}
Additionally, connection-oriented systems are blocking by default, that is, only
one client at a time is supported (line busy).

The addressing protocol IP supports both connection-oriented and connection-less
protocols on top.
Can you name an example for each?

%When viewing connection-oriented systems we have another issue to deal with,
%however: As soon as someone connects, the whole system is blocked.
%It would be rather bad if only one person could access a website at a time.
%To solve that, such a servers address (the number) is actually not the one of
%the service proper but could be seen as a secretary:  Picking up phone calls
%and then forwarding/delegating them to the actual recipient.
%This way the system is only blocked as long as the \quote{secretary} needs to
%forward the \quote{call}.
%
%%
%\startplacefigure[title=Connection-Oriented Sockets with a Listener]
%\hbox{%
%\starttikzpicture[auto]
%\node[draw] (srv) {listener};
%\node[draw,above left=of srv] (w1)  {worker-1};
%\node[draw,left=of srv] (w2)  {worker-1};
%\node[below left=of srv] (w3)  {\dots};
%\node[draw,above right=of srv] (cl1) {client-1};
%\node[draw,right=of srv] (cl2) {client-2};
%\node[below right=of srv] (cl3) {\dots};
%
%\draw[->,dashed] (cl1) edge node[above] {(1)} (srv);
%\draw[->] (cl1) edge node[above] {(2)} (w1);
%\draw[->,dotted] (srv) -- (w1);
%\draw[->] (cl2) edge node {(3)} (srv);
%\stoptikzpicture
%}
%\stopplacefigure
%
\placefloats
\stopsubject

\startsubject[title=UNIX Domain Sockets]

We can use a server/client architecture for IPC between different processes on
the same system.
In UNIX, the server creates a special file (a
socket) that doesn't really have \quote{content} on the disk.
The client then specifies the filename or path to
the servers socket (the servers \quote{phone number}) in order to connect.
Both processes can then read and write arbitrarily to the \quote{file},
and each would receive the other partys messages.

In this exercise you should create a connection-oriented server
which accepts a connection over UNIX domain sockets by one client at a time.
The client can enter messages which will be displayed
at the servers side.
\startplacelisting[title=UNIX Server \& Client]
\startnocode
$ ./server server.socket
Server is listening on 'server.socket' ...
\stopnocode
\startnocode
$ ./client server.socket
Client connecting to 'server.socket' ...
Your Message: 
\stopnocode
\stopplacelisting
%
\placefloats
\stopsubject

\startsubject[title=Berkeley Sockets API]

To program virtually any server and client program on UNIX we use the
Berkeley Sockets API\@.
This API allows for different protocols \quote{below}.
We will now go through the steps you need to setup a server and a
client with a connection-oriented approach.

Both, server and client create one socket signifying the endpoints
of the connection.
To create a socket we use the function
\goto{socket(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/socket.html)]
which takes three arguments, the domain (address family, eg.\@ \type{AF_INET} for
IPv4 or \type{AF_UNIX} for UNIX), 
the type (\type{SOCK_STREAM} for connection-oriented or \type{SOCK_DGRAM} for
connection-less) and the addressing protocol family,
which is usually the same value as the address family and can be set to \type{0}
in that case.\footnote{%
  Actually, the appropriate value for the \type{AF_UNIX} domain would be
  \type{PF_UNIX}, but both constants are often equal.
  Just using the default of \type{0} for the protocol is best-practice anyhow.
}
%\startplacelisting[title=Server \& Client: Create a socket]
%\startccode
%int s = socket(AF_UNIX, SOCK_STREAM, 0);
%\stopccode
%\stopplacelisting
After server and client created a socket (which, in the API, is simply a file
descriptor stored as an \type{int}), the code is different in the server and
client program.

The server needs to bind the abstract socket to a concrete address
(\type{struct sockaddr}) with the appropriate value (a filename in our case)
and call
\goto{bind(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/bind.html)].
%
%\startplacelisting[title=Server: Binding to an address]
%\startccode
%bind(s, &address, sizeof (address);
%\stopccode
%\stopplacefigure
%
Afterwards the server needs to start to
\goto{listen(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/listen.html)]
on our socket.
Here we also need to
specify a number of additional allowed connections to the same socket while we
are busy talking to one connection, the so-called backlog.
%
%\startplacelisting[title=Server: Start listening]
%\startccode
%listen(s, 0);
%\stopccode
%\stopplacelisting
%

The clients equivalent to bind \& listen is initiating a connection.
In a similar way as we bound the socket to an address for the server, we now
create \type{struct sockaddr} with the same content for the client to
\goto{connect(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/connect.html)]
to.
%\startplacelisting[title=Client: Initiate connection]
%\startccode
%connect(s, &address, sizeof (address));
%\stopccode
%\stopplacelisting

Now that the connection is established, both server and client can simply
\goto{read(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/read.html)]
and
\goto{write(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/write.html)]
to their respective sockets to receive or send data.
You cannot use \type{fread}, \type{fwrite} or even \type{fprintf} here but need
to use these low-level interfaces we worked with in the first exercise.
%
\startplacefigure[title=UNIX IPC with Berkeley Sockets]
\hbox{%
\starttikzpicture[auto,node distance=2.7cm]

\node[draw,label={180:\type{socket()}}] (ss) {\type{s}};
\node[draw,right=of ss,fill=white] (file) {\type{srv.socket}};
\node[draw,right=of file,label={  0:\type{socket()}}] (cs) {\type{s}};

\node[node distance=1cm,above=of ss] (srv) {{\bf Server}};
\node[node distance=1cm,above=of cs] (clt) {{\bf Client}};

\draw[<->] (ss) edge node[above] {\type{bind()}} node[below] {\type{listen()}} (file);
\draw[<->] (cs) edge node[above] {\type{connect()}} (file);

\node[below=of ss] (srw) {msg};
\node[below=of cs] (crw) {msg};

\draw[<->] (ss) edge node {\type{read()}/\type{write()}} (srw);
\draw[<->] (cs) edge node {\type{read()}/\type{write()}} (crw);

\path (srv) -- coordinate[midway] (hi) (clt);
\path (srw) -- coordinate[midway] (lo) (crw);

\startscope[on background layer]
\draw[dashed] (hi) -- (lo);
\stopscope
\stoptikzpicture
}
\stopplacefigure

To close the connection you can simply
\goto{close(3)}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/close.html)]
the socket/\hskip0pt filedescriptor.\footnote{%
  Actually you could keep the fd and only call \type{shutdown} but \type{close}
  does that internally for us already.
}

\placefloats
\stopsubject

\stopproduct
