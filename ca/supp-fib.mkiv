\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Recursion}]

\startsubject[title={The Callstack}]

We've already covered regular jumps (conditional and unconditional) before.
While these are useful to implement control structures, these jumps are
uni-directional, once jumped we have no way to tell where we've been
\quote{before}, where we came from.
There are scenarios however, where this information would be useful, i.e., when
we want to jump back to that point.
One example is our wrapper which \emph{called} your function(s), and after these
were done, the execution \emph{returned} where our wrapper left of.
This effectively \quote{pauses} the execution of the \emph{caller} while the
\emph{callee} does its work.

This is what the \type{call} and \type{ret} instructions are used for.
\startplacelisting[reference=fig:call,title={Call \& Return}]
\startasmcode[escape={[[,]]}]
foo:    mov rdi, 42
        [[\tikz[overlay,remember picture] \node (call) {};]]call bar 
        ; the following instruction will be
        ; executed after bar has returned
        mov rbx, rax[[\tikz[overlay,remember picture] \node (aftercall) {};]]
        ; ...

bar:    [[\tikz[overlay,remember picture] \node (bar) {};]]mov rax, rdi
        add rax, 10
        ret[[\tikz[overlay,remember picture] \node (ret) {};]]
        ; an instruction after this won't be
        ; executed.[[\starttikzpicture[overlay,remember picture]\draw[->,color=blue] (call) to[bend right] (bar);\draw[->,color=red] (ret) to[bend right,in=300,out=300,looseness=2] (aftercall);\stoptikzpicture]]
\stopasmcode
\stopplacelisting

Let's look at the exmaple above a bit more closely to the way the CPU sees it.
\startplacefigure
\hbox{%
\starttikzpicture[instr/.style={draw,node distance=0cm,minimum height=1.5em}]
\node[instr]            (a) {\type{mov rdi, 42}};
\node[instr,right=of a] (c) {\type{call 0x2000}};
\node[instr,right=of c] (d) {\type{mov rbx, rax}};
\node[instr,right=of d] (e) {\ldots};
\node[instr,right=of e] (f) {\type{mov rax, rdi}};
\node[instr,right=of f] (g) {\type{add rax, 10}};
\node[instr,right=of g] (i) {\type{ret}};
\node[instr,right=of i] (j) {\ldots};


\draw[->,blue] (c.north) [bend left] to (f.north);
\draw[->,red] (i.north) [bend right] to (d.north);


\draw[] (a.south west) to ++(0,-0.5) node[below] {$0\mathrm{x}1000$};
\draw[] (c.south west) to ++(0,-0.5) node[below] {$0\mathrm{x}1005$};
\draw[] (d.south west) to ++(0,-0.5) node[below] {$0\mathrm{x}1010$};
\draw[] (f.south west) to ++(0,-0.5) node[below] {$0\mathrm{x}2000$};
\draw[] (g.south west) to ++(0,-0.5) node[below] {$0\mathrm{x}2003$};
\draw[] (i.south west) to ++(0,-0.5) node[below] {$0\mathrm{x}2008$};

\stoptikzpicture
}
\stopplacefigure

The first thing to note is that the labels are gone and the call to \type{bar}
replaced by the actual address of the first instruction at that position.
We can observe, that in this example the address of the instruction to be
executed after the \type{ret} at address $0\mathrm{x}2008$ is $0\mathrm{x}1010$.

What, however, happens, when not only \type{foo} calls \type{bar} but \type{bar}
in turn calls \type{baz}?  In order to understand that and how our CPU memorizes
the position of the caller, we need to look at the stack.
While we know that our program is residing somewhere in memory, obviously other
things, like data, occupy some main memory as well.
In a typical Linux program, the memory of our program in memory looks roughly
like shown in \in{Figure}[fig:elf], with all of our code being in the
\type{section .text}.
\startplacefigure[reference=fig:elf,title={x86 UNIX Program Memory Layout}]
\hbox{%
\starttikzpicture[block/.style={node distance=0cm,draw,minimum height=1.5em,text width=3cm,align=center}]

\node[block,fill=grey!10] (a) {};
\node[block,below=of a]   (b) {\type{.text}};
\node[block,below=of b]   (c) {\type{.data}};
\node[block,below=of c]   (d) {\type{.bss}};
\node[block,below=of d]   (e) {Heap};
\node[block,fill=grey!10,below=of e,minimum height=2cm] (f) {(unallocated)};
\node[block,below=of f]   (g) {Stack};
\node[block,fill=grey!10,below=of g] (h) {};

\draw (a.north west) to ++(-0.5,0) node[left] {$0\mathrm{x}0$};
\draw (g.north west) to ++(-0.5,0) node[left] {\type{rsp}};

\draw[|->] (e.south) to ++(0,-0.5);
\draw[|->] (g.north) to ++(0, 0.5);
\stoptikzpicture
}
\stopplacefigure

We could (and will) put constant data in the \type{.data} section as well
as reserve some (uninitialized) memory for use in \type{.bss} in other exercises.
But for now we will look at the stack which, in x86, grows from the higher
addresses \quote{down} towards the lower addresses, into a (hopefully) big
chunk of yet unallocated memory.
The current amount of stack regarded as allocated is controlled by the value of
a register we already named but specifically avoided to use, the \type{rsp} or
\emph{stack pointer}.
Allocating another \unit{4 B} is simple as decreasing \type{rsp} by \digits{4},
but let's first have a more detailed look at the stack itself, from the
perspective of our program.

\startplacefigure[reference=fig:callstack,title={Callstack}]
\startcombination[3*1] % x columns, y rows
{\hbox{%
\starttikzpicture[
  block/.style={
    draw,
    minimum height=1.5em,
    text width=3cm,
    node distance=0cm,
    align=center
  }
]

\node[block           ]   (a) {???};
\node[block,above=of a]   (b) {$0\mathrm{x}30fa$};
\node[block,above=of b,fill=grey!10,minimum height=6em]   (c) {};

\draw[|->] (b.north) to ++(0, 0.5);

\stoptikzpicture
}} {before call to bar}
%
{\hbox{%
\starttikzpicture[
  block/.style={
    draw,
    minimum height=1.5em,
    text width=3cm,
    node distance=0cm,
    align=center
  }
]

\node[block           ]   (a) {???};
\node[block,above=of a]   (b) {$0\mathrm{x}30fa$};
\node[block,above=of b]   (c) {$0\mathrm{x}1010$};
\node[block,above=of c,fill=grey!10,minimum height=4.5em]   (d) {};

\draw[|->] (c.north) to ++(0, 0.5);

\stoptikzpicture
}} {within bar}
%
{\hbox{%
\starttikzpicture[
  block/.style={
    draw,
    minimum height=1.5em,
    text width=3cm,
    node distance=0cm,
    align=center
  }
]

\node[block           ]   (a) {???};
\node[block,above=of a]   (b) {$0\mathrm{x}30fa$};
\node[block,above=of b,fill=grey!10,minimum height=6em]   (c) {};

\draw[|->] (b.north) to ++(0, 0.5);

\stoptikzpicture
}} {after ret again}
%
\stopcombination
\stopplacefigure

We can see that upon program entry, e.g., with \type{rip} at address
$0\mathrm{x}1000$ the top \quote{element} (we for now assume each element having
a size of \unit{8 Byte}, more on that later) of our stack is the value
$0\mathrm{x}30fa$.
However, just after we executed the \type{call} instruction, the value
$0\mathrm{x}1010$ is suddenly at the top of the stack.
Incidentally, this is also the address of instruction \emph{after the call}, i.e.,
the address where the execution shall resume.
We can thus think of the call instruction as doing two things:  \quote{pushing}
the current \type{rip} onto the stack and then jumping to the target---in fact,
that's indeed all that it does.
The \type{ret} instruction simply \quote{pops} the top element of the stack
and replaces the \type{rip} with this value, thus \quote{jumping back}.

Now let's think about the question of what happened if \type{bar} called a third
\emph{function}, perhaps called \type{baz}?  Well, simply another element is
added to our stack, containing the \emph{return address} of that call.
Hence, a \type{ret} instruction always returns to the instruction after the
\emph{last} \type{call} instruction executed, making nested function calls possible.

We can now guess, that the address $0\mathrm{x}30fa$ is, in fact, an address
belonging to the piece of code that has called \type{foo}.
\stopsubject


\startsubject[title={Other uses of the stack}]

While in our simple case only return addresses were pushed on the stack, the
stack has far more uses than that.
Especially in the early days of x86 with far less registers, programmers soon
ran out of space and thus had to store their values somewhere else than in the
registers.

Say the register \type{rax} holds some crucial value for later that we however
don't need right now.
What we could use, however, would be another \quote{free} register.
We can help ourselves by storing the value in \type{rax} on the stack;  now
can freely use it again.
After we are done with our calculations and having stored the result somewhere,
we simply restore the value from the stack!
%
\startplacelisting
\startasmcode
foo:    mov rax, rdi
        mov rcx, 1
        mov rdx, 2
        ; ...
        ; all registers in use :(
        push rax
        ; rax is backed up and can be
        ; used for other calculations again
        mov rax, 42
        ; ...
        ; done with use of rax, restore its
        ; old value again 
        pop rax
        ; ...
        ret
\stopasmcode
\stopplacelisting
%
What if we forgot to restore the value from the stack?  Well, for one we'd have
\quote{lost} the value that was so crucial we did a backup.
But also, we've greatly confused our computer as the \type{ret} instruction will
now assume that this very value is the return address (as it's the top of the
stack) and jump to whatever value we set \type{rax} to before pushing it on the
stack.
It is thus \emph{very} important to remember to clean-up the stack after again,
after using it!
\stopsubject


\startsubject[title={(Non-)Volatile Registers}]

We're finally in the position to understand why volatile and non-volatile registers
are so important, and why we weren't allowed to use the latter yet!
If \type{foo} calls \type{bar} nothing happens except that first, the \type{rip}
is pushed and then is set to the target address.
This means that all register values stay the same as they are \quote{shared}.
In fact, that's precisely how we passed our arguments to functions, we simply
stored them in some registers which, by convention, are used for that purpose.
Since the calle\emph{e} has access to the very same registers, it can read, but also
modify, these values.

However, most likely, our calle\emph{r} wants some values to persist, i.e., not
to be changed by the calle\emph{e}.
We've just learned that, of course, they could simply push every register value
they cared about before calling.
This is called \quote{caller saving} and definitely works---however memory
operations are costly, and if we can avoid them we do.
%
\startplacelisting[title={Using volatile/caller-saved registers}]
\startasmcode
foo:    ; ...
        mov rax, 42     ; very important!
        ; ...
        push rax        ; backup
        call bar        ; might modify rax
        pop rax         ; restored
        ; ...
        ret
\stopasmcode
\stopplacelisting

Instead, some registers are designated as \emph{non-volatile} or
\quote{calle\emph{e}-saved}.
These registers \emph{must} be hold the same value after call as they had before.
Thus, when calling a function ourselves, we could simply store the values we want
to persist into a non-volatile register, such as \type{rbx}, right?

\startplacelisting[title={calle\emph{e}-saved}]
\startasmcode
foo:    push rbx        ; backup non-vol reg
        ; ...
        mov rax, 42     ; very important!
        ; ...
        mov rbx, rax    ; backup
        call bar        ; might modify rax
        mov rax, rbx    ; restored
        ; ...
        pop rbx         ; restore non-vol reg
        ret
\stopasmcode
\stopplacelisting

Unfortunately, we, ourselves, are calle\emph{e}s to another function as well,
so by using \type{rbx} we violate the convention to keep \type{rbx} to the same
value, too.
The only way out of this is to use the stack, thus in order to use them in our
program we need to back \emph{them} up (usually at the start of our code).
They are then completely free to use in our program, we just have to remember
to restore them at the end.

One important thing to remember:  If we push $a$, then $b$ and then $c$, we
need to pop these values in \emph{reverse order} since the top of the stack
contains $c$, then $b$ and just then $a$!
\stopsubject


\startsubject[title={Recursion}]

Till now \type{foo} has called \type{bar} and this has, in turn, maybe called
\type{baz}.
We can however program a function which calls itself recursively as part of
its algorithm.
In this case we need to make extensive care to correctly use volatile/non-volatile
registers as our cale\emph{e} obviously uses (and thus modifies) the same
registers as the calle\emph{r}---they are the same after all.

Let's write a simple \type{sum(n)} function that sums all numbers from $[0,n]$.
For those who have experience in Haskell, there this could look like this:
\starthaskellcode
mysum :: Integer -> Integer
mysum 0 = 0
mysum n = mysum (n-1) + n
\stophaskellcode 

Our code will thus first compare whether our value is $0$, if yes, we can just
return $0$.  Otherwise we need to backup our $n$ (to the stack or some non-volatile
register), and pass $n-1$ in \type{rdi} as argument, then call ourselves again.
After the call, we can assume \type{rax} to hold the value of $0+\cdots+(n-1)$,
so we simply need to add the backed up $n$ to \type{rax} and return.

We choose to store our value in \type{rbx}, so the very first thing we do is
push that register onto the stack.
\startasmcode
sum:        push rbx
            ; todo
.end:       pop rbx
            ret
\stopasmcode

Our program should no \quote{fail successfully} in that it runs, doesn't
crash, but most definitely gives the wrong result.
The next step is to compare our input against $0$:
\startasmcode
            cmp rdi, 0
            je .anchor
.recursion: ; todo
            jmp .end

.anchor:    mov rax, 0
            jmp .end
\stopasmcode

The last step is to do the \quote{actual work}, backup, recursive call, restore
and addition:
\startasmcode
.recursion: mov rbx, rdi
            sub rdi, 1
            call sum
            add rax, rbx
            jmp .end
\stopasmcode

And that's it!  Surely not the first efficient way to implement the recursion,
but following this approach should make it easier to write your own recursive
functions, even if more complex.
\stopsubject

\stopproduct
