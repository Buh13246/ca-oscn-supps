.POSIX:

IMAGE=registry.gitlab.com/islandoftex/images/context:current
PODMAN=podman run
PODMANFLAGS=--rm -v $(PWD):$(PWD):Z -w $(PWD)/$(@D) --env OSFONTDIR=$(PWD)/fonts 
CONTEXT=$(PODMAN) $(PODMANFLAGS) $(IMAGE) context
CONTEXTFLAGS=--paranoid --nonstopmode

PRODSRC_CA=\
        ca/supp-fpcalc.mkiv    \
        ca/supp-collatz.mkiv   \
        ca/supp-hfpu.mkiv      \
        ca/supp-fib.mkiv       \
        ca/supp-simd.mkiv      \
        ca/supp-gauss.mkiv     \
        ca/supp-hofs.mkiv      \
        ca/supp-sort.mkiv      \
        ca/supp-convert.mkiv   \
        ca/supp-cat.mkiv       \
        ca/supp-matrix.mkiv
PRODSRC_OSCN=\
        oscn/supp-cat2.mkiv         \
        oscn/supp-C-intro.mkiv      \
        oscn/supp-ls.mkiv           \
        oscn/supp-fserv.mkiv        \
        oscn/supp-scheduling.mkiv   \
        oscn/supp-httpd.mkiv        \
        oscn/supp-bc.mkiv           \
        oscn/supp-tcp.mkiv          \
        oscn/supp-signals.mkiv      \
        oscn/supp-sockets.mkiv      \
        oscn/supp-mm.mkiv
PRODPDF_CA=$(PRODSRC_CA:.mkiv=.pdf)
PRODPDF_OSCN=$(PRODSRC_OSCN:.mkiv=.pdf)
PRODPDF=$(PRODPDF_CA) $(PRODPDF_OSCN)

all: $(PRODPDF_CA) $(PRODPDF_OSCN)

.mkiv.pdf:
	if [ "$<" = "proj.mkiv" ] || [ "$<" = "env.mkiv" ]; then \
	    printf "Can't build proj.pdf"; \
	    exit 1; \
	fi
	$(CONTEXT) $(CONTEXTFLAGS) $(<F)

clean:
	rm -f *.pdf *.tuc *.log *.pgf *.synctex

.SUFFIXES:
.SUFFIXES: .mkiv .pdf
