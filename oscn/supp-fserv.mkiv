\startproduct supp-fserv
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title=Supplement: TCP Fileserver]

\startsubject[title=A (very) simple Fileserver protocol]

We can now send messages over TCP back and forth, why not put this to good use?
In this exercise you are tasked to only write a server (as that is the intersting
part).

Specifically, a file allows a client to connect and send a message containing
a filename.
The server than looks up the specified file and opens it (if it exists), then
copies the content through the filedescriptor of the socket to the client.
You should already have the groundwork for creating the TCP based server, but
you also know how to copy files through filedescriptors---you did just that in
the \type{cat(1)} exercise at the beginning of the semester!

The only difference is that back then you used the functions \type{read(3)} and
\type{write(3)} while now we use \type{recv(3)} and \type{send(3)}.
In fact, these functions are equivalent, or more precisely, \type{recv(3)} and
\type{send(3)} with their last argument set to zero behave exactly as \type{read(3)}
and \type{write(3)} respectively do:
\startccode
read(fd, buf, nbyte) ~ recv(fd, buf, nbyte, 0);
write(fd, buf, nbyte) ~ send(fd, buf, nbyte, 0);
\stopccode
So all you need to do is put those two parts together: Server creates and
listens, then accepts a connection, the child \type{recv(3)}s the filename
from the client, \type{open(3)}s the file and copies it to the socket just the
way we did before.

Remember though, that if you enter a message on the client (be it your own,
\type{nc(1)} or \type{telnet(1)}) and hit the \type{ENTER} key, this will not
only send the filename but also append a \type{\n} character!  Instead you want
to use \type{^D} (\type{Control+D}) to send the ASCII EOT character, submitting
the filename to the server without appending \type{\n}.
%
\placefloats
\stopsubject

\startsubject[title=IPv4 and IPv6 (Optional)]

Our current implementation only supports IPv4, and while this will likely
co-exist with the newer IPv6 standard for quite some time, we want to be future
proof.
This goes a little beyond the bare minimum of the task but we highly
recommend you to continue reading ;)

A big goal in software development is to be \quote{portable}.  That means that
our code can run on different platforms, be it Windows, Linux, macOS, FreeBSD,
or, like now, IPv4 and IPv6.
Ideally, we don't really need to write two almost identical versions of the
program or deal with details of either protocol.
The key to enable portability is abstraction, we've covered a few portable
abstractions already.
Sockets are an abstraction over network interfaces, file descriptors an
abstraction over things (not necessarily files) \quote{that can be written to
and read from}.
We profited from those well-designed abstractions as well, the TCP communication was
almost identical to the UNIX IPC communication and now our fileserver pushing a
file over network shares a big deal of code with a program that simply prints a
file to the screen---it's the same thing, as far as the abstraction is concerned.

And thus, of course, there exists a function that abstracts IPv4 and IPv6.\footnote{%
  Actually even DNS\@.
}
This function is called \goto{\type{getaddrinfo(3)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/getaddrinfo.html)] and is meant to be used as a
replacement to filling the \type{struct sockaddr} structure manually.

We can have many different kinds of connections, listen/server type,
IPv4 and IPv6 based connections, TCP or UDP, etc.\@ and combinations
thereof.
This new function allows us to \quote{filter} all possible connections by fixing
some as constraints.
In this exercise we want to allow for IPv4 and IPv6 but restrict ourselves to
TCP\@.

We do that by filling a structure introduced by \type{getaddrinfo}, called
\type{struct addrinfo}.  This structure has members for each \quote{constraint}
we may want to have:
\type{ai_flags} (listen/server mode vs.\@ connect/client),
\type{ai_family} (IPv4 or IPv6),
\type{ai_socktype} (STREAM or DGRAM, i.e.\@ TCP or UDP),
and \type{ai_protocol} (again IPv4 or IPv6).\footnote{%
  And some more, but we don't care about them that much.
}
%
\startplacelisting[title=Setting the hints for \type{getaddrinfo(3)}]
\startccode
struct addrinfo hints = {
	.ai_flags    = /* ... */,
	.ai_family   = /* ... */,
	.ai_socktype = /* ... */,
	.ai_protocol = /* ... */,
	.ai_next     = NULL,
};
\stopccode
\stopplacelisting
%
Each member can now be filled by their respective values (e.g.\@ \type{SOCK_STREAM}
for \type{ai_socktype}) or left unspecified by setting it to a special value.
Quoting from POSIX:
\startquotation
  A value of \type{AF_UNSPEC} for \type{ai_family} means that the caller shall
  accept any address family.
  A value of zero for \type{ai_socktype} means that the caller shall accept any
  socket type.
  A value of zero for \type{ai_protocol} means that the caller shall accept any
  protocol.
\stopquotation
After we specified our \quote{hints}/\hskip0pt constraints, we can ask
\type{getaddrinfo(3)} to lookup all possible configurations which fulfill these,
given a specific address and port.
E.g.\@ for listening on \emph{any available IP address}, port 5000:\footnote{%
  You can also specify e.g.\@ IPv4 localhost, but this will obviously restrict us to
  IPv4, regardless of the settings in \type{hints}.
}
\startccode
struct addrinfo infos;
getaddrinfo(NULL, "5000", &hints, &infos);
\stopccode
Now the \type{infos} structure will contain the first element of a linked-list
of entries that all fit the description.

We can then simply iterate over the structure, creating sockets for all those
matching descriptions as seen in \in{Listing}[lst:getaddrinfo-iter].
%
\startplacelisting[reference=lst:getaddrinfo-iter,title=Iterating over addrinfo]
\startccode
struct addrinfo *p;
for (p = info; p != NULL; p = p->ai_next) {
	/* access p->ai_* */
}
\stopccode
\stopplacelisting
%
In this exercise we will, however, it suffices to use the first entry in the
list that works.
We do that by trying to create a socket and binding it:
\startccode
int s = socket(p->ai_family, p->ai_socktype,
	p->ai_protocol);
bind(s, p->ai_addr, p->ai_addrlen);
\stopccode
If either fails (error handling!), we try the next \type{info} entry.

After we successfully bound a socket to an address we can discard the \type{info}
structure and carry on exactly like we did in all the exercises before.
That's it!

We can use the same functions for clients as well, the only difference is that
\type{ai_flags} is set to a different value.
Also this makes it quite easy to connect to other services as \type{getaddrinfo}
doesn't only accept IPs as its first argument but also domains!  You can do:
\startccode
getaddrinfo("www.hoogle.com", "80", &hints, &infos);
\stopccode


\stopsubject


\stopproduct
